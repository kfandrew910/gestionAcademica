package clasesyobjetos;

import java.time.LocalDate;

public class Alumno {

	/*
	 * Funciones especificas:
	 * 
	 * Constructores(todos los atributos, default, NIA, copia)
	 * 
	 * getters y setters
	 * 
	 * leerDatos(clave y otros datos)
	 * 
	 * mostrarDatos(nombre y otros datos)
	 * 
	 * */
	
	private String NIA; //clave
	private String DNI;
	private String nombre;
	private String apellidos;
	private LocalDate fechaNacimiento;
	
}
