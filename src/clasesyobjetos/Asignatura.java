package clasesyobjetos;

import daw.com.Pantalla;
import daw.com.Teclado;

public class Asignatura {
	
	/*
	 * Funciones especificas:
	 * 
	 * Constructores(todos los atributos, default, copia)
	 *  
	 * getters y setters
	 * 
	 * leerDatos
	 * 
	 * MostrarDatos
	 * 
	 * */
	
	private String nombre;
	private String codigo;
	
	public Asignatura() {
		this("");
	}
	
	public Asignatura(String nombre) {
		this(nombre,"");
	}
	
	public Asignatura(String nombre,String codigo) {
		this.nombre=nombre;
		this.codigo=codigo;
	}
	
	public Asignatura(Asignatura or) {
		this(or.nombre,or.codigo);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
	public void leerDatos() {
		leerClave();
		leerOtrosDatos();
	}

	public void leerClave() {
		codigo=Teclado.leerString("\nCodigo");
		
	}

	public void leerOtrosDatos() {
		
		nombre=Teclado.leerString("\nNombre");
	}

	@Override
	public String toString() {
		return "Asignatura [nombre=" + nombre + ", codigo=" + codigo + "]";
	}
	
	
	public void mostrarDatos() {
		Pantalla.escribirString("\n"+this);
	}
	
	
	public boolean equals(Asignatura otro) {
		return otro!=null&&otro.codigo.equals(codigo);
	}
	
	
	
	
}
